/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.scene.Node;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Vector3f;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;


/**
 *
 * @author Matt
 */
public class AIAppState extends AbstractAppState {

    private SimpleApplication app;
    private BulletAppState bullet;
    private AppStateManager stateManager;
    private LinkedList<AIComponent> AIList;
    private Random points = new Random();
    private Node charModel = new Node();
    private Vector3f pos = new Vector3f(0, 0, 0);
    private int kills = 0;
    private float levelMultiplier = 1f;
    private int level = 0;
    private int zSpawns = 0;
    private int mod;

    

    @Override
    public void initialize(AppStateManager manager, Application app) {

        super.initialize(manager, app);
        this.app = (SimpleApplication) app;
        stateManager = this.app.getStateManager();
        bullet = this.stateManager.getState(BulletAppState.class);
        AIList = new LinkedList<AIComponent>();
        setUpAI();
        mod = app.getStateManager().getState(Scene.class).getSpawnList().size();
        
    }

    public void setUpAI() {
        
        this.charModel = (Node) app.getAssetManager().loadModel("Models/ZombieA/Zombie.mesh.j3o");       
        
    }

    @Override
    public void update(float tpf) {
        for (ListIterator<AIComponent> AI = AIList.listIterator(); AI.hasNext();) {
            AIComponent aiComp = AI.next();
            aiComp.updateAI(tpf);
            if (aiComp.getDeath() == true){
                AI.remove();
                kills++;
            }

        }
        
        if (zSpawns < (level * level) && AIList.size() < 50) {
            Vector3f temp;
            int index = (Math.abs(points.nextInt() % mod));
            temp = app.getStateManager().getState(Scene.class).getSpawnList().get(index).clone();
            temp.x += (points.nextInt() % 5 - points.nextInt() % 10);
            temp.z += (points.nextInt() % 5 - points.nextInt() % 10);
            AIComponent AIComp = new AIComponent(charModel.clone(false), temp.clone(), app, bullet);
            AIComp.setHealth(100 + (level*level));
            app.getStateManager().getState(AIAppState.class).getAIList().add(AIComp);
            zSpawns++;
        }
        if (AIList.size() <= 0) {
            level++;
            zSpawns = 0;
        }

    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        for (ListIterator<AIComponent> AI = AIList.listIterator(); AI.hasNext();) {
            AIComponent aiComp = AI.next();
            aiComp.setEnabled(enabled);
            }
    }

   
 

    public int getKills() {
        return kills;
    }

    public void setPos(Vector3f pos) {
        this.pos = pos;
    }
    
    public int getLevel(){
        return level;
    }
    
    public int getNumAI(){
        return AIList.size();
    }
    
    public List getAIList(){
        return AIList;
    }

    
}
