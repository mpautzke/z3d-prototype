/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Vector3f;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 *
 * @author matt
 */
public class ParticleAppState extends AbstractAppState {
    
    private SimpleApplication app;
    private BulletAppState bullet;
    private AppStateManager stateManager;
    private LinkedList<ParticleComponent> particleList;
    private ParticleComponent particleComponent;
    
    
     @Override
    public void initialize(AppStateManager manager, Application app) {
        
        super.initialize(manager, app);
        this.app = (SimpleApplication) app;
        stateManager = this.app.getStateManager();
        bullet = this.stateManager.getState(BulletAppState.class);
        particleList = new LinkedList<ParticleComponent>();
        particleComponent = new ParticleComponent(app);
    }
     
@Override
    public void update(float tpf){
         
            for (ListIterator<ParticleComponent> it = particleList.listIterator(); it.hasNext();) {
                ParticleComponent partComp = it.next();
                partComp.setDuration(tpf);
                if (partComp.getDuration() > 2){
                    partComp.setKillBlood();
                    it.remove();
                }
            }
            
        
    }

        public void CreateBlood(Vector3f Location){
        particleComponent = new ParticleComponent(app);
        particleComponent.setEmitBlood(Location.clone());
        particleList.add(particleComponent);
        
    }
        
        public void CreateSplatter(Vector3f Location){
        particleComponent = new ParticleComponent(app);
        particleComponent.setEmitSplatter(Location.clone());
        particleList.add(particleComponent);
        }


}
