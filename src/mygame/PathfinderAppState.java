/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Vector3f;
import java.util.ArrayList;
import java.util.List;
import jme3tools.navmesh.NavMeshPathfinder;
import jme3tools.navmesh.Path;

/**
 *
 * @author matt
 */
public class PathfinderAppState extends AbstractAppState {
    private SimpleApplication app;
    private NavMeshPathfinder Pathfinder;
    private Vector3f org = new Vector3f(0, 0, 0);

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication) app;
        Pathfinder = new NavMeshPathfinder(stateManager.getState(Scene.class).getNavMesh());
    }
    
    public List findPath(Vector3f Origin, Vector3f Target) {

        org = Pathfinder.warpInside(Origin.clone());
        Pathfinder.setPosition(org);
        Vector3f tar = Pathfinder.warpInside(Target.clone());
        Pathfinder.computePath(tar);
        List<Vector3f> wpPositions = new ArrayList<Vector3f>();
        for (Path.Waypoint wp : Pathfinder.getPath().getWaypoints()) {
            wpPositions.add(wp.getPosition().clone());
        }
        return wpPositions;
    }
    
}
