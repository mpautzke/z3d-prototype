/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.*;
import com.jme3.math.*;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 *
 * @author Matt
 */
public class BlltAppState extends AbstractAppState {

    private SimpleApplication app;
    private BulletAppState bullet;
    private AppStateManager stateManager;
    private LinkedList<BlltComponent> blltList;
    private BlltComponent blltComponent;
    
    

    @Override
    public void initialize(AppStateManager manager, Application app) {
        
        super.initialize(manager, app);
        this.app = (SimpleApplication) app;
        stateManager = this.app.getStateManager();
        bullet = this.stateManager.getState(BulletAppState.class);
        blltList = new LinkedList<BlltComponent>();
        blltComponent = new BlltComponent(app, bullet);
    }
    
    @Override
    public void update(float tpf){
         
            for (ListIterator<BlltComponent> it = blltList.listIterator(); it.hasNext();) {
                BlltComponent blltComp = it.next();
                blltComp.setDuration(tpf);
                if (blltComp.getDuration() > 2){
                    
                    it.remove();
                    
                }
            }
            
        
    }
    
    public void CreateBullet(Vector3f Location, Vector3f Direction){
        blltComponent = new BlltComponent(app, bullet);
        blltComponent.Bullet(Location.clone(), Direction.clone());
        blltList.add(blltComponent);
        
    }
    
    public void CreateBomb(Vector3f Location, Vector3f Direction){
        blltComponent = new BlltComponent(app, bullet);
        blltComponent.bomb(Location, Direction);
            blltList.add(blltComponent);
    }
    

}
