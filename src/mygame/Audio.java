/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.scene.Node;

/**
 *
 * @author Matt
 */
public class Audio {
    private AudioNode pistol;
    private AudioNode shotgun;
    private AudioNode game;
    private AudioNode menu;
    private AudioNode scream;
    private AudioNode run;
    private AudioNode button;
    private AudioNode zburst;
    
    public void initAudio(Node rootNode,AssetManager assetManager) {
    /* gun shot sound is to be triggered by a mouse click. */
    pistol = new AudioNode(assetManager, "Sounds/pistol.wav", false);
    pistol.setLooping(false);
    pistol.setReverbEnabled(false);
    pistol.setVolume(.5f);
    pistol.setChannel(0);
    pistol.setPositional(true);
    
    shotgun = new AudioNode(assetManager, "Sounds/shotgun.wav", false);
    shotgun.setLooping(false);
    shotgun.setReverbEnabled(false);
    shotgun.setPositional(true);
    shotgun.setVolume(.5f);
    shotgun.setPositional(true);
 
    /* nature sound - keeps playing in a loop. */
    game = new AudioNode(assetManager, "Sounds/wind.wav", false);
    game.setLooping(true);  // activate continuous playing
    game.setPositional(false);
    game.setVolume(.5f);
    
    menu = new AudioNode(assetManager, "Sounds/musicbox.wav", false);
    menu.setLooping(true);
    menu.setReverbEnabled(false);
    menu.setPositional(true);
    menu.setVolume(1f);

    scream = new AudioNode(assetManager, "Sounds/scream.wav", false);
    scream.setReverbEnabled(false);
    scream.setPositional(true);

    run = new AudioNode(assetManager, "Sounds/run.wav", false);
    run.setPositional(true);
    run.setMaxDistance(1000);
    run.setRefDistance(5);
    run.setReverbEnabled(false);

    button = new AudioNode(assetManager, "Sounds/deep.wav", false);
    button.setReverbEnabled(false);
    button.setPositional(false);

    zburst = new AudioNode(assetManager, "Sounds/zburst.wav", false);
    zburst.setReverbEnabled(false);
    zburst.setPositional(true);
    zburst.setVolume(1f);

  }
    public AudioNode getPistol(){
        return pistol;
    }
    public AudioNode getShotgun(){
        return shotgun;
    }
    public AudioNode getGame(){
        return game;
    }
    public AudioNode getMenu(){
        return menu;
    }
    public AudioNode getScream(){
        return scream;
    }
    public AudioNode getRun(){
        return run;
    }
    public AudioNode getButton(){
        return button;
    }
    public AudioNode getBurst(){
        return zburst;
    }
    
}
