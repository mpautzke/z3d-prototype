/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.scene.Node;
import com.jme3.asset.AssetManager;

/**
 *
 * @author Matt
 */
public class Character {

    private Node model;

    public Node robot(AssetManager assetManager) {
        return model = (Node) assetManager.loadModel("Models/Oto/Oto.mesh.xml");
    }

    public Node Sinbad(AssetManager assetManager) {
        return model = (Node) assetManager.loadModel("Models/Sinbad/Sinbad.mesh.xml");
    }

    public Node Ninja(AssetManager assetManager) {
        return model = (Node) assetManager.loadModel("Models/Ninja/Ninja.mesh.xml");
    }

    public Node Dude(AssetManager assetManager) {
        model = (Node) assetManager.loadModel("Models/Dude/dudeMesh.mesh.j3o");
        //model.setMaterial(assetManager.loadMaterial(null));
        return model;
    }

    public Node Zombie(AssetManager assetManager) {
        model = (Node) assetManager.loadModel("Models/ZombieA/Zombie.mesh.j3o");

        //model.setMaterial(assetManager.loadMaterial(null));
        return model;
    }
//          public Node Zombie2(AssetManager assetManager){
//         model = (Node) assetManager.loadModel("Models/ZombieB/Zombie.mesh.xml");
//         
//         //model.setMaterial(assetManager.loadMaterial(null));
//         return model;
    //}
}
