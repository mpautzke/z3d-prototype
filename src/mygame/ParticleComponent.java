/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.effect.shapes.EmitterSphereShape;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;

/**
 *
 * @author matt
 */
public class ParticleComponent {

    private SimpleApplication app;
    private ParticleEmitter muzzleEmitter, bloodEmitter, dustEmitter, splatterEmitter, shockwave, debris;
    private float duration;
    private AudioNode splatter;

    public ParticleComponent(Application app) {

        this.app = (SimpleApplication) app;
        initEmitters();
        splatter = Main.audio.getBurst();
    }

    private void initEmitters() {
        Material debris_mat = new Material(app.getAssetManager(),
                "Common/MatDefs/Misc/Particle.j3md");

        debris_mat.setTexture("Texture", app.getAssetManager().loadTexture(
                "Textures/Effects/DebrisA.png"));
        debris_mat.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Off);
        debris_mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
        bloodEmitter = new ParticleEmitter("Emitter", ParticleMesh.Type.Triangle, 10);
        bloodEmitter.setMaterial(debris_mat);
        bloodEmitter.setSelectRandomImage(true);
        bloodEmitter.setRandomAngle(true);
        bloodEmitter.setImagesX(3);
        bloodEmitter.setImagesY(3); // 2x2 texture animation
        bloodEmitter.setEndColor(new ColorRGBA(1f, 0f, 0f, 1f));   // red
        bloodEmitter.setStartColor(new ColorRGBA(1f, 0f, 0f, 0f)); // yellow
        bloodEmitter.setStartSize(2f);
        bloodEmitter.setEndSize(.2f);
        bloodEmitter.setGravity(0, 30f, 0);
        bloodEmitter.setLowLife(2f);
        bloodEmitter.setHighLife(.5f);
        bloodEmitter.setParticlesPerSec(0.0f);
        bloodEmitter.getParticleInfluencer().setVelocityVariation(0.6f);


        //bloodEmitter.setParticlesPerSec(0);

        muzzleEmitter = new ParticleEmitter("Emitter", ParticleMesh.Type.Triangle, 10);
        muzzleEmitter.setMaterial(debris_mat);
        muzzleEmitter.setImagesX(3);
        muzzleEmitter.setImagesY(3); // 2x2 texture animation
        muzzleEmitter.setEndColor(new ColorRGBA(1f, 1f, 0f, 1f));   // red
        muzzleEmitter.setStartColor(new ColorRGBA(1f, 1f, 0f, 1f)); // yellow
        muzzleEmitter.getParticleInfluencer().setInitialVelocity(new Vector3f(20f, 20f, 20f));
        muzzleEmitter.setStartSize(0.3f);
        muzzleEmitter.setEndSize(0.0f);
        muzzleEmitter.setGravity(0, 0, 0);
        muzzleEmitter.setLowLife(.01f);
        muzzleEmitter.setHighLife(.1f);
        muzzleEmitter.setParticlesPerSec(0.0f);
        muzzleEmitter.setInWorldSpace(false);
        muzzleEmitter.getParticleInfluencer().setVelocityVariation(0.3f);
        muzzleEmitter.setParticlesPerSec(0);

        Material mate = new Material(app.getAssetManager(),
                "Common/MatDefs/Misc/Particle.j3md");

        splatterEmitter = new ParticleEmitter("Flame", Type.Triangle, 100);
        splatterEmitter.setSelectRandomImage(true);
        splatterEmitter.setStartColor(new ColorRGBA(.2f, 0f, 0f, 1f));
        splatterEmitter.setEndColor(new ColorRGBA(.5f, 0f, 0f, 0f));
        splatterEmitter.setStartSize(1.3f);
        splatterEmitter.setEndSize(2f);
        splatterEmitter.setShape(new EmitterSphereShape(Vector3f.ZERO, 2f));
        splatterEmitter.setParticlesPerSec(0);
        splatterEmitter.setGravity(0, 30f, 0);
        splatterEmitter.setLowLife(1f);
        splatterEmitter.setHighLife(1.5f);
        splatterEmitter.setInitialVelocity(new Vector3f(20, 20, 20));
        splatterEmitter.setVelocityVariation(5f);
        splatterEmitter.setImagesX(2);
        splatterEmitter.setImagesY(2);
        mate.setTexture("Texture", app.getAssetManager().loadTexture("Textures/Effects/flame.png"));
        mate.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
        mate.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
        splatterEmitter.setMaterial(mate);

        shockwave = new ParticleEmitter("Shockwave", Type.Triangle, 1);
//        shockwave.setRandomAngle(true);
        shockwave.setFaceNormal(Vector3f.UNIT_Y);
        shockwave.setStartColor(new ColorRGBA(.48f, 0.17f, 0.01f, 1f));
        shockwave.setEndColor(new ColorRGBA(.48f, 0.17f, 0.01f, 1f));
        shockwave.setStartSize(0f);
        shockwave.setEndSize(5f);
        shockwave.setParticlesPerSec(0);
        shockwave.setGravity(0, 0, 0);
        shockwave.setLowLife(0.5f);
        shockwave.setHighLife(0.5f);
        shockwave.setInitialVelocity(new Vector3f(0, 0, 0));
        shockwave.setVelocityVariation(0f);
        shockwave.setImagesX(1);
        shockwave.setImagesY(1);
        Material shock = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Particle.j3md");
        shock.setTexture("Texture", app.getAssetManager().loadTexture("Textures/Effects/shockwave.png"));
        shock.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
        shock.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
        shockwave.setMaterial(shock);

        debris = new ParticleEmitter("Debris", Type.Triangle, 30);
        debris.setSelectRandomImage(true);
        debris.setRandomAngle(true);
        debris.setRotateSpeed(FastMath.TWO_PI * 4);
        debris.setStartColor(ColorRGBA.White);
        debris.setEndColor(ColorRGBA.White);
        debris.setStartSize(.5f);
        debris.setEndSize(.5f);

        debris.setShape(new EmitterSphereShape(Vector3f.ZERO, 1f));
        debris.setParticlesPerSec(0);
        debris.setGravity(0, 30f, 0);
        debris.setLowLife(.5f);
        debris.setHighLife(1.5f);
        debris.setImagesX(3);
        debris.setImagesY(3);
        Material deb = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Particle.j3md");
        deb.setTexture("Texture", app.getAssetManager().loadTexture("Textures/Effects/Zdebris.png"));
        deb.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
        deb.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
        debris.setMaterial(deb);

    }

    public void setEmitBlood(Vector3f Location) {

        bloodEmitter.getParticleInfluencer().setInitialVelocity(new Vector3f(5f, 3f, 5f));
        bloodEmitter.getParticleInfluencer().setVelocityVariation(.6f);
        app.getRootNode().attachChild(bloodEmitter);
        bloodEmitter.setLocalTranslation(Location);
        bloodEmitter.emitAllParticles();
    }

    public void setEmitSplatter(Vector3f Location) {
        splatterEmitter.getParticleInfluencer().setInitialVelocity(new Vector3f(5f, 3f, 5f));
        splatterEmitter.getParticleInfluencer().setVelocityVariation(2f);
        app.getRootNode().attachChild(splatterEmitter);
        splatterEmitter.setLocalTranslation(Location);
        splatterEmitter.emitAllParticles();

        shockwave.getParticleInfluencer().setInitialVelocity(new Vector3f(0f, 0f, 0f));
        shockwave.getParticleInfluencer().setVelocityVariation(0f);
        app.getRootNode().attachChild(shockwave);
        shockwave.setLocalTranslation(Location);
        shockwave.emitAllParticles();

        debris.getParticleInfluencer().setInitialVelocity(new Vector3f(5f, 3f, 5f));
        debris.getParticleInfluencer().setVelocityVariation(5f);
        app.getRootNode().attachChild(debris);
        debris.setLocalTranslation(Location);
        debris.emitAllParticles();
        
        splatter.setLocalTranslation(Location);
        splatter.playInstance();

    }

    public void setKillBlood() {

        if (bloodEmitter != null) {
            bloodEmitter.killAllParticles();
            app.getRootNode().detachChild(bloodEmitter);
        }
        if (shockwave != null) {
            shockwave.killAllParticles();
            app.getRootNode().detachChild(shockwave);
        }
        if (debris != null) {
            debris.killAllParticles();
            app.getRootNode().detachChild(debris);
        }
        if (splatterEmitter != null) {
            splatterEmitter.killAllParticles();
            app.getRootNode().detachChild(splatterEmitter);
        }
    }

    public void setDuration(float Dur) {
        duration += Dur;
    }

    public float getDuration() {
        return duration;
    }
}
