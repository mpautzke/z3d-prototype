/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.animation.SkeletonControl;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AppStateManager;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import java.util.Random;

/**
 *
 * @author matt
 */
public class GunManager{
    
    private SimpleApplication app;
    private BlltAppState blltsAppState;
    private Node pistol;
    private Node Shotgun;
    private AudioNode aShotgun;
    private AudioNode aPistol;
    private Node Hand;
    private String Current;
    private Random ran = new Random();
    private SkeletonControl skelControl;
    
    public void GunManager(Application app, AppStateManager manager){
           this.app = (SimpleApplication) app;
           this.blltsAppState = manager.getState(BlltAppState.class);
           
           pistol = (Node) app.getAssetManager().loadModel("Models/Gun/Gun.mesh.j3o");
           pistol.scale(1.2f);
           pistol.move(-.5f, 1.5f, 0f);
           aPistol = Main.audio.getPistol();
           pistol.attachChild(aPistol);
           
           Shotgun = (Node) app.getAssetManager().loadModel("Models/Shotgun/ShotGun.mesh.j3o");
           Shotgun.scale(1.2f);
           Shotgun.move(-.5f, 1.0f, 0f);
           aShotgun = Main.audio.getShotgun().clone();
           Shotgun.attachChild(aShotgun);
           skelControl = app.getStateManager().getState(UserAppState.class).getPlayerModel().getControl(SkeletonControl.class);
           initEmitters();
    }
    
    private ParticleEmitter muzzleEmitter;
    
     private void initEmitters() {
        muzzleEmitter = new ParticleEmitter("EmitterBox", ParticleMesh.Type.Triangle, 10);
        Material debris_mat = new Material(app.getAssetManager(),
                "Common/MatDefs/Misc/Particle.j3md");

        debris_mat.setTexture("Texture", app.getAssetManager().loadTexture(
                "Textures/Effects/Debris.png"));

        muzzleEmitter.setMaterial(debris_mat);
        
        muzzleEmitter.setImagesX(3);
        muzzleEmitter.setImagesY(3); // 2x2 texture animation
        muzzleEmitter.setEndColor(new ColorRGBA(1f, 1f, 0f, 1f));   // red
        muzzleEmitter.setStartColor(new ColorRGBA(1f, 1f, 0f, 1f)); // yellow
        muzzleEmitter.getParticleInfluencer().setInitialVelocity(new Vector3f(5f, 3f, 5f));
        muzzleEmitter.setStartSize(0.3f);
        muzzleEmitter.setEndSize(0.0f);
        muzzleEmitter.setGravity(0, 0, 0);
        muzzleEmitter.setLowLife(.01f);
        muzzleEmitter.setHighLife(.1f);
        muzzleEmitter.setParticlesPerSec(0.0f);
        muzzleEmitter.setInWorldSpace(false);
        muzzleEmitter.getParticleInfluencer().setVelocityVariation(0.6f);
        muzzleEmitter.setParticlesPerSec(0);

        //bloodEmitter.setParticlesPerSec(0);

    }
     
     public void shoot(){
         
         if (Current.equals("pistol")){
             aPistol.playInstance();
             blltsAppState.CreateBullet(pistol.getWorldTranslation(), pistol.getWorldRotation().getRotationColumn(1));
             muzzleEmitter.killAllParticles();
             pistol.detachChild(muzzleEmitter);
             muzzleEmitter.getParticleInfluencer().setInitialVelocity(new Vector3f(5f, 3f, 5f));
             muzzleEmitter.getParticleInfluencer().setVelocityVariation(.6f);
             pistol.attachChild(muzzleEmitter);
             muzzleEmitter.setLocalTranslation(0f,1f,0);
             muzzleEmitter.emitAllParticles();
         }
         
         if (Current.equals("shotgun")){
             aShotgun.playInstance();
             float temp = ran.nextFloat();
             blltsAppState.CreateBullet(Shotgun.getWorldTranslation(), Shotgun.getWorldRotation().getRotationColumn(1).add(new Vector3f(1,1,1).multLocal(temp%.2f)));
             blltsAppState.CreateBullet(Shotgun.getWorldTranslation(), Shotgun.getWorldRotation().getRotationColumn(1).add(new Vector3f(1,1,0).multLocal((temp+=.51f)%.2f)));
             blltsAppState.CreateBullet(Shotgun.getWorldTranslation(), Shotgun.getWorldRotation().getRotationColumn(1).add(new Vector3f(1,0,1).multLocal((temp+=.51f)%.2f)));
             blltsAppState.CreateBullet(Shotgun.getWorldTranslation(), Shotgun.getWorldRotation().getRotationColumn(1).add(new Vector3f(0,1,1).multLocal((temp+=.51f)%.2f)));
             blltsAppState.CreateBullet(Shotgun.getWorldTranslation(), Shotgun.getWorldRotation().getRotationColumn(1).add(new Vector3f(1,0,0).multLocal((temp+=.51f)%.2f)));
             blltsAppState.CreateBullet(Shotgun.getWorldTranslation(), Shotgun.getWorldRotation().getRotationColumn(1).add(new Vector3f(0,1,0).multLocal((temp+=.51f)%.2f)));
             blltsAppState.CreateBullet(Shotgun.getWorldTranslation(), Shotgun.getWorldRotation().getRotationColumn(1).add(new Vector3f(0,0,1).multLocal((temp+=.51f)%.2f)));
             
             muzzleEmitter.killAllParticles();
             Shotgun.detachChild(muzzleEmitter);
             muzzleEmitter.getParticleInfluencer().setInitialVelocity(new Vector3f(5f, 3f, 5f));
             muzzleEmitter.getParticleInfluencer().setVelocityVariation(.6f);
             Shotgun.attachChild(muzzleEmitter);
             muzzleEmitter.setLocalTranslation(0f,1f,0);
             muzzleEmitter.emitAllParticles();
         }
         
     }
     
     public void setGun(String Current){
         this.Current = Current;
         if (this.Current.equals("pistol")){
         Hand = skelControl.getAttachmentsNode("Ulna.R");
         Hand.detachAllChildren();
         Hand.attachChild(pistol);

         }
         if (this.Current.equals("shotgun")){
         Hand = skelControl.getAttachmentsNode("Ulna.R");
         Hand.detachAllChildren();
         Hand.attachChild(Shotgun);
         
         }
     }
     public String getGun(){
         return Current;
     }
    
}
