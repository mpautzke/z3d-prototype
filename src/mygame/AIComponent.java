/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.control.KinematicRagdollControl;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 *
 * @author normenhansen
 */
public final class AIComponent implements PhysicsCollisionListener {
    //System

    private SimpleApplication app;
    private BulletAppState bullet;
    //AI Character
    private Node AI;
    private Node Model;
    private KinematicRagdollControl ragdoll;
    private ParticleAppState partAppState;
    //AI Stats
    private Vector3f Position;
    private boolean death = false;
    private float Health = 100;
    private float deathDuration = 0;
    private float duration = 0;
    private AIControl AiControl;

    public AIComponent(Node Model, Vector3f startPostion, Application app, BulletAppState bullet) {
        this.app = (SimpleApplication) app;
        this.bullet = bullet;
        this.Model = Model;
        this.Position = startPostion;
        partAppState = app.getStateManager().getState(ParticleAppState.class);
        setUpAI();
        // path = app.getStateManager().getState(PathfinderAppState.class).findPath(getPos(), app.getStateManager().getState(UserAppState.class).getPosition());

    }

    public void setUpAI() {
        AI = new Node("AI");
        AI.setUserData("Health", Health);
        Model.move(0f, -2.5f, 0f);
        Model.setShadowMode(ShadowMode.CastAndReceive);
        Model.setCullHint(Spatial.CullHint.Dynamic);

        AiControl = new AIControl(Position, app, Model);
        AI.addControl(AiControl);

        AI.attachChild(Model);
        app.getRootNode().attachChild(AI);

        bullet.getPhysicsSpace().addCollisionListener(this);
    }

    public void updateAI(float tpf) {
        
        if (!AiControl.isEnabled()) {
            
            death = true;

        }

    }

    public void collision(PhysicsCollisionEvent event) {
        switch (event.getType()) {

            case PhysicsCollisionEvent.TYPE_PROCESSED:
                final Spatial nodeA = event.getNodeA();
                final Spatial nodeB = event.getNodeB();

                if (nodeA == null || nodeB == null) {
                    return;
                }

                if (AI.equals(nodeA) || AI.equals(nodeB)) {
                    if ("bullet".equals(nodeA.getName()) || "bullet".equals(nodeB.getName())
                            || "Missile".equals(nodeA.getName()) || "Missile".equals(nodeB.getName())) {

                        if (AiControl.isEnabled()) {
                            Health -= 100;
                            if (Health <= 0) {

                                AiControl.setEnabled(false);
                                AI.removeControl(AiControl);
                                partAppState.CreateSplatter(event.getPositionWorldOnB());
                                app.getRootNode().detachChild(AI);


                            }
                        }

                        partAppState.CreateBlood(event.getPositionWorldOnB());

                    }

                }


                break;
        }
    }
    
    

    public boolean getDeath() {
        return death;
    }

    public float getDuration() {
        return duration;
    }

    public void setEnabled(boolean enabled) {
        AiControl.setEnabled(enabled);
    }
    public void setHealth(int Health){
        this.Health = Health;
    }
    
}
