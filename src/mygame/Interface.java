/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.niftygui.NiftyJmeDisplay;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.EffectBuilder;
import de.lessvoid.nifty.builder.ImageBuilder;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.controls.dropdown.builder.DropDownBuilder;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.examples.controls.chatcontrol.ChatControlDialogDefinition;
import de.lessvoid.nifty.examples.controls.common.CommonBuilders;
import de.lessvoid.nifty.examples.controls.common.DialogPanelControlDefinition;
import de.lessvoid.nifty.examples.controls.common.MenuButtonControlDefinition;
import de.lessvoid.nifty.examples.controls.dragndrop.DragAndDropDialogDefinition;
import de.lessvoid.nifty.examples.controls.dropdown.DropDownDialogControlDefinition;
import de.lessvoid.nifty.examples.controls.listbox.ListBoxDialogControlDefinition;
import de.lessvoid.nifty.examples.controls.scrollpanel.ScrollPanelDialogControlDefinition;
import de.lessvoid.nifty.examples.controls.sliderandscrollbar.SliderAndScrollbarDialogControlDefinition;
import de.lessvoid.nifty.examples.controls.textfield.TextFieldDialogControlDefinition;
import de.lessvoid.nifty.screen.Screen;

/**
 *
 * @author Matt
 */
public class Interface {

    public static InterfaceController startScreen;
    private static CommonBuilders builders = new CommonBuilders();

    public void createMenu(SimpleApplication app) {
        startScreen = Main.startScreen;
        NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(
                app.getAssetManager(), app.getInputManager(), app.getAudioRenderer(), app.getGuiViewPort());
        Nifty nifty = niftyDisplay.getNifty();
        app.getGuiViewPort().addProcessor(niftyDisplay);

        nifty.loadStyleFile("nifty-default-styles.xml");
        nifty.loadControlFile("nifty-default-controls.xml");

        // register some helper controls
        MenuButtonControlDefinition.register(nifty);
        DialogPanelControlDefinition.register(nifty);

        // register the dialog controls
        ListBoxDialogControlDefinition.register(nifty);
        DropDownDialogControlDefinition.register(nifty);
        ScrollPanelDialogControlDefinition.register(nifty);
        ChatControlDialogDefinition.register(nifty);
        TextFieldDialogControlDefinition.register(nifty);
        SliderAndScrollbarDialogControlDefinition.register(nifty);
        DragAndDropDialogDefinition.register(nifty);

        createMainMenuScreen(nifty);
        createSinglePlayerScreen(nifty);
        createSettingsScreen(nifty);
        createHudScreen(nifty);
        createPauseScreen(nifty);
        createDeathScreen(nifty);
        nifty.gotoScreen("menu");

    }

    private void createMainMenuScreen(Nifty nifty) {
        Screen main = new ScreenBuilder("menu") {
            {
                controller(startScreen);


                layer(new LayerBuilder("background") {
                    {
                        childLayoutCenter();

                        // add image
                        image(new ImageBuilder() {
                            {

                                filename("Interface/Zombie3.png");
                                height("100%");
                                width("100%");
                            }
                        });
                    }
                });

                layer(new LayerBuilder("foreground") {
                    {
                        childLayoutVertical();


                        // panel added
                        panel(new PanelBuilder("panel_top") {
                            {
                                childLayoutCenter();
                                alignCenter();
                                height("25%");
                                width("75%");


                            }
                        });

                        panel(new PanelBuilder("panel_mid") {
                            {
                                childLayoutVertical();
                                alignCenter();
                                height("50%");
                                width("75%");
                                padding("200px");

                                panel(builders.vspacer("20px"));

                                control(new ButtonBuilder("SinglePlayerButton", "Single Player") {
                                    {
                                        height("30px");
                                        width("100px");
                                        alignCenter();
                                        visibleToMouse(true);
                                        interactOnClick("ButtonSound()");
                                        interactOnRelease("startGame(singleplayer)");

                                    }
                                });
                                panel(builders.vspacer("20px"));
                                control(new ButtonBuilder("SettingsButton", "Settings") {
                                    {
                                        height("30px");
                                        width("100px");
                                        alignCenter();
                                        visibleToMouse(true);
                                        interactOnClick("ButtonSound()");
                                        interactOnRelease("startGame(settings)");

                                    }
                                });
                                panel(builders.vspacer("20px"));
                                control(new ButtonBuilder("QuitButton", "Quit") {
                                    {
                                        height("30px");
                                        width("100px");

                                        alignCenter();
                                        visibleToMouse(true);
                                        interactOnClick("ButtonSound()");
                                        interactOnRelease("quitGame()");

                                    }
                                });

                            }
                        });

                        panel(new PanelBuilder("panel_bottom") {
                            {
                                childLayoutHorizontal();
                                alignCenter();
                                height("25%");
                                width("75%");

                                panel(new PanelBuilder("panel_bottom_left") {
                                    {
                                        childLayoutCenter();
                                        valignCenter();
                                        height("50%");
                                        width("50%");

                                        // add button control


                                    }
                                });

                                panel(new PanelBuilder("panel_bottom_right") {
                                    {
                                        childLayoutCenter();
                                        valignCenter();
                                        height("50%");
                                        width("50%");

                                        // add button control


                                    }
                                });
                            }
                        }); // panel added
                    }
                });
                layer(new LayerBuilder("blackOverlay") {
                    {
                        onCustomEffect(new EffectBuilder("renderQuad") {
                            {
                                customKey("onResolutionStart");
                                length(500);
                                neverStopRendering(false);
                            }
                        });
                        onStartScreenEffect(new EffectBuilder("renderQuad") {
                            {

                                length(500);
                                effectParameter("startColor", "#000000");
                                effectParameter("endColor", "#0000");
                            }
                        });
                        onEndScreenEffect(new EffectBuilder("renderQuad") {
                            {
                                length(500);
                                effectParameter("startColor", "#0000");
                                effectParameter("endColor", "#000000");

                            }
                        });
                    }
                });

            }
        }.build(nifty);
    }

    private void createSinglePlayerScreen(Nifty nifty) {

        Screen singleplayer = new ScreenBuilder("singleplayer") {
            {
                controller(startScreen);


                layer(new LayerBuilder("background") {
                    {
                        childLayoutCenter();

                        // add image
                        image(new ImageBuilder() {
                            {

                                filename("Interface/Zombie3.png");
                                height("100%");
                                width("100%");
                            }
                        });
                    }
                });

                layer(new LayerBuilder("foreground") {
                    {
                        childLayoutVertical();


                        // panel added
                        panel(new PanelBuilder("panel_top") {
                            {
                                childLayoutCenter();
                                alignCenter();
                                height("25%");
                                width("75%");

                                // add text

                            }
                        });

                        panel(new PanelBuilder("panel_mid") {
                            {
                                childLayoutCenter();
                                alignCenter();
                                height("50%");
                                width("75%");

                            }
                        });

                        panel(new PanelBuilder("panel_bottom") {
                            {
                                childLayoutHorizontal();
                                alignCenter();
                                height("25%");
                                width("75%");

                                panel(new PanelBuilder("panel_bottom_left") {
                                    {
                                        childLayoutCenter();
                                        valignCenter();
                                        height("50%");
                                        width("50%");

                                        // add button control
                                        control(new ButtonBuilder("StartButton", "Start") {
                                            {
                                                alignCenter();
                                                valignCenter();
                                                height("50%");
                                                width("50%");
                                                visibleToMouse(true);
                                                interactOnClick("ButtonSound()");
                                                interactOnRelease("startGame(hud)");

                                            }
                                        });

                                    }
                                });

                                panel(new PanelBuilder("panel_bottom_right") {
                                    {
                                        childLayoutCenter();
                                        valignCenter();
                                        height("50%");
                                        width("50%");

                                        // add button control
                                        control(new ButtonBuilder("BackButton", "Back") {
                                            {
                                                alignCenter();
                                                valignCenter();
                                                height("50%");
                                                width("50%");
                                                visibleToMouse(true);
                                                interactOnClick("ButtonSound()");
                                                interactOnRelease("startGame(menu)");
                                            }
                                        });

                                    }
                                });
                            }
                        }); // panel added
                    }
                });
                layer(new LayerBuilder("blackOverlay") {
                    {
                        onCustomEffect(new EffectBuilder("renderQuad") {
                            {
                                customKey("onResolutionStart");
                                length(500);
                                neverStopRendering(false);
                            }
                        });
                        onStartScreenEffect(new EffectBuilder("renderQuad") {
                            {
                                length(500);
                                effectParameter("startColor", "#000000");
                                effectParameter("endColor", "#0000");
                            }
                        });
                        onEndScreenEffect(new EffectBuilder("renderQuad") {
                            {
                                length(500);
                                effectParameter("startColor", "#0000");
                                effectParameter("endColor", "#000000");

                            }
                        });
                    }
                });

            }
        }.build(nifty);
    }

    private void createSettingsScreen(Nifty nifty) {

        Screen settings = new ScreenBuilder("settings") {
            {
                controller(startScreen);


                layer(new LayerBuilder("background") {
                    {
                        childLayoutCenter();

                        // add image
                        image(new ImageBuilder() {
                            {

                                filename("Interface/Zombie3.png");
                                height("100%");
                                width("100%");
                            }
                        });
                    }
                });

                layer(new LayerBuilder("foreground") {
                    {
                        childLayoutVertical();

                        panel(new PanelBuilder("panel_mid") {
                            {
                                childLayoutCenter();
                                alignLeft();
                                height("50%");
                                width("75%");

                                panel(new PanelBuilder() {
                                    {
                                        childLayoutHorizontal();
                                        control(builders.createLabel("DropDown:"));
                                        control(new DropDownBuilder("dropDown") {
                                            {
                                                width("*");
                                            }
                                        });
                                    }
                                });

                            }
                        });

                        panel(new PanelBuilder("panel_bottom_right") {
                            {
                                childLayoutCenter();
                                valignCenter();
                                height("50%");
                                width("50%");

                                // add button control
                                control(new ButtonBuilder("BackButton", "Back") {
                                    {

                                        height("10%");
                                        width("10%");
                                        visibleToMouse(true);
                                        interactOnClick("ButtonSound()");
                                        interactOnRelease("startGame(menu)");
                                    }
                                });

                            }
                        });


                    }
                });
                layer(new LayerBuilder("blackOverlay") {
                    {
                        onCustomEffect(new EffectBuilder("renderQuad") {
                            {
                                customKey("onResolutionStart");
                                length(500);
                                neverStopRendering(false);
                            }
                        });
                        onStartScreenEffect(new EffectBuilder("renderQuad") {
                            {
                                length(500);
                                effectParameter("startColor", "#000000");
                                effectParameter("endColor", "#0000");
                            }
                        });
                        onEndScreenEffect(new EffectBuilder("renderQuad") {
                            {
                                length(500);
                                effectParameter("startColor", "#0000");
                                effectParameter("endColor", "#000000");

                            }
                        });
                    }
                });

            }
        }.build(nifty);
    }

    private void createHudScreen(Nifty nifty) {
        Screen hud = new ScreenBuilder("hud") {
            {
                controller(startScreen);

                layer(new LayerBuilder("background") {
                    {
                        childLayoutCenter();

                        // add image
//            image(new ImageBuilder() {{
//                filename("Interface/hud-frame.png");
//            }});

                    }
                });

                layer(new LayerBuilder("foreground") {
                    {
                        childLayoutHorizontal();

                        // panel added
                        panel(new PanelBuilder("panel_left") {
                            {
                                childLayoutVertical();
                                height("100%");
                                width("80%");
                                // <!-- spacer -->
                            }
                        });

                        panel(new PanelBuilder("panel_right") {
                            {
                                childLayoutVertical();

                                height("100%");
                                width("20%");

                                panel(new PanelBuilder("panel_top_right1") {
                                    {
                                        childLayoutVertical();
                                        height("15%");
                                        width("100%");

                                        panel(new PanelBuilder("panel_top") {
                                            {
                                                childLayoutHorizontal();
                                                height("15%");
                                                width("100%");

                                                // add label control
                                                control(new LabelBuilder() {
                                                    {

                                                        id("label");
                                                        color("#00FF00");
                                                        text("Health:");

                                                    }
                                                });
                                                control(new LabelBuilder() {
                                                    {
                                                        id("health");
                                                        color("#00FF00");
                                                        text("100");


                                                    }
                                                });

                                            }
                                        });
                                    }
                                });
                                panel(new PanelBuilder("panel_top_right2") {
                                    {
                                        childLayoutVertical();
                                        height("15%");
                                        width("100%");

                                        panel(new PanelBuilder("panel_mid") {
                                            {
                                                childLayoutHorizontal();
                                                height("15%");
                                                width("100%");

                                                // add label control
                                                control(new LabelBuilder() {
                                                    {
                                                        id("label2");
                                                        color("#FF0000");
                                                        text("Kills:");

                                                        height("100%");
                                                    }
                                                });

                                                control(new LabelBuilder() {
                                                    {
                                                        id("kills");
                                                        color("#FF0000");
                                                        text("0");

                                                        height("100%");
                                                    }
                                                });

                                            }
                                        });
                                    }
                                });
                                panel(new PanelBuilder("panel_top_right3") {
                                    {
                                        childLayoutVertical();
                                        height("15%");
                                        width("100%");

                                        panel(new PanelBuilder("panel_bot") {
                                            {
                                                childLayoutHorizontal();
                                                height("15%");
                                                width("100%");

                                                // add label control

                                                control(new LabelBuilder() {
                                                    {
                                                        id("label3");
                                                        color("#0000FF");
                                                        text("Level:");

                                                        height("100%");
                                                    }
                                                });

                                                control(new LabelBuilder() {
                                                    {
                                                        id("level");
                                                        color("#0000FF");
                                                        text("0");

                                                        height("100%");
                                                    }
                                                });

                                            }
                                        });



                                    }
                                });

                            }
                        }); // panel added
                    }
                });

                layer(new LayerBuilder("whiteOverlay") {
                    {
                        onCustomEffect(new EffectBuilder("renderQuad") {
                            {
                                customKey("onResolutionStart");
                                length(500);
                                neverStopRendering(false);
                            }
                        });
                        onStartScreenEffect(new EffectBuilder("renderQuad") {
                            {
                                length(500);
                                effectParameter("startColor", "#000000");
                                effectParameter("endColor", "#0000");
                            }
                        });
//                                        onEndScreenEffect(new EffectBuilder("renderQuad") {
//                                            {
//                                                length(500);
//                                                effectParameter("startColor", "#0000");
//                                                effectParameter("endColor", "#000000");
//
//                                            }
//                                        });
                    }
                });
            }
        }.build(nifty);
    }

    private void createPauseScreen(Nifty nifty) {
        Screen pause = new ScreenBuilder("pause") {
            {
                controller(startScreen);

                layer(new LayerBuilder("background") {
                    {
                        childLayoutCenter();

                        // add image
//            image(new ImageBuilder() {{
//                filename("Interface/hud-frame.png");
//            }});

                    }
                });

                layer(new LayerBuilder("foreground") {
                    {
                        childLayoutHorizontal();

                        // panel added
                        panel(new PanelBuilder("panel_left") {
                            {
                                childLayoutVertical();
                                height("100%");
                                width("80%");
                                // <!-- spacer -->
                            }
                        });

                        panel(new PanelBuilder("panel_right") {
                            {
                                childLayoutVertical();
                                height("100%");
                                width("20%");

                                panel(new PanelBuilder("panel_top_right1") {
                                    {
                                        childLayoutCenter();
                                        height("15%");
                                        width("100%");

                                        control(new ButtonBuilder("ResumeButton", "Resume") {
                                            {
                                                alignCenter();
                                                valignCenter();
                                                height("50%");
                                                width("50%");
                                                visibleToMouse(true);
                                                interactOnClick("hud");
                                            }
                                        });

                                    }
                                });

                                panel(new PanelBuilder("panel_bot_right") {
                                    {
                                        childLayoutCenter();
                                        valignCenter();
                                        height("70%");
                                        width("100%");

                                    }
                                });
                            }
                        }); // panel added
                    }
                });
            }
        }.build(nifty);
    }

    private void createDeathScreen(Nifty nifty) {
        Screen death = new ScreenBuilder("death") {
            {
                controller(startScreen);

                layer(new LayerBuilder("background") {
                    {
                        childLayoutCenter();

                        // add image
                        image(new ImageBuilder() {
                            {
                                filename("Interface/GameOver.png");
                            }
                        });

                    }
                });

                layer(new LayerBuilder("foreground") {
                    {
                        childLayoutHorizontal();

                        // panel added
                        panel(new PanelBuilder("panel_left") {
                            {
                                childLayoutVertical();
                                height("100%");
                                width("80%");
                                // <!-- spacer -->
                            }
                        });

                        panel(new PanelBuilder("panel_right") {
                            {
                                childLayoutVertical();
                                height("100%");
                                width("20%");

                                panel(new PanelBuilder("panel_top_right1") {
                                    {
                                        childLayoutCenter();
                                        height("15%");
                                        width("100%");

                                    }
                                });

                                panel(new PanelBuilder("panel_bot_right") {
                                    {
                                        childLayoutCenter();
                                        valignCenter();
                                        height("70%");
                                        width("100%");

                                    }
                                });
                            }
                        }); // panel added
                    }
                });
            }
        }.build(nifty);

    }
}
