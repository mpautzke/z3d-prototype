/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Texture;

/**
 *
 * @author matt
 */
public class Decals extends AbstractAppState {

    SimpleApplication app;
    private Texture tex;
    private Material mat;
    private Geometry bHole;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication) app;
        tex = this.app.getAssetManager().loadTexture("com/jme3/app/Monkey.png");
        mat = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setTexture("ColorMap", tex);
        mat.setColor("Color", new ColorRGBA(1f,1f,1f,1f));
        mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);


    }

    public void placeBulletHole(Vector3f pos, Vector3f normal) {
                        Quad q = new Quad(.5f, .5f);                        
                        bHole = new Geometry("quad", q);
                        bHole.setMaterial(mat);

                        Quaternion qa = new Quaternion();
                        qa.lookAt(normal, Vector3f.UNIT_Y);
                        bHole.setLocalRotation(qa);
                        pos.y +=.01;
                        
                        bHole.setLocalTranslation(pos);
                        app.getRootNode().attachChild(bHole);
    }

   
}