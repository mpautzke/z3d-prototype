package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.*;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.scene.shape.Sphere;
import com.jme3.scene.Geometry;
import com.jme3.material.Material;
import com.jme3.math.*;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.FogFilter;
import com.jme3.shadow.PssmShadowFilter;
import com.jme3.shadow.PssmShadowRenderer;
import com.jme3.shadow.PssmShadowRenderer.CompareMode;
import com.jme3.shadow.PssmShadowRenderer.FilterMode;
import com.jme3.water.WaterFilter;
import com.jme3.post.filters.BloomFilter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * @author Matt
 */
public class Main extends SimpleApplication {

    public static Keyboard keyboard = new Keyboard();
    public static Audio audio = new Audio();
    public static Interface menu = new Interface();
    public static InterfaceController startScreen;
    public static BulletAppState bulletAppState;
    public static UserAppState userAppState;
    public static AIAppState AIAppState;
    public static BlltAppState blltsAppState;
    public static ParticleAppState partAppState;
    public static PathfinderAppState pathfinderAppState;
    public static LevelManager levelManager;
    public static Text text = new Text();
    public static Scene scene;
    public static Character character = new Character();
    public static Vector3f lightDir;
    private FilterPostProcessor fpp;
    private FogFilter fog;
    private PssmShadowRenderer pssmRenderer;
    private PssmShadowFilter pssmFilter;
    private BloomFilter bloom = new BloomFilter();

    public static void main(String[] args) throws IOException {


        com.jme3.system.AppSettings settings = new com.jme3.system.AppSettings(false);

        settings.setTitle("Zombie Cubed");
        settings.setSettingsDialogImage("Interface/Zombie3.png");
        //settings.setIcons(new BufferedImage[]{ImageIO.read(new File("assets/Interface/Zombie3.bmp"))});
        Main app = new Main();
        app.setSettings(settings);
        app.start();

    }

    @Override
    public void simpleUpdate(float tpf) {
        //forester.update(tpf);

        // font color
        if (userAppState.getDeath() == true) {
            startScreen.startGame("death");
        }

    }

    public void simpleInitApp() {
        keyboard.setUpKeys(inputManager);
        audio.initAudio(rootNode, assetManager);

        bulletAppState = new BulletAppState();
        bulletAppState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);
        stateManager.attach(bulletAppState);

        scene = new Scene();
        stateManager.attach(scene);

        blltsAppState = new BlltAppState();
        stateManager.attach(blltsAppState);

        userAppState = new UserAppState();
        stateManager.attach(userAppState);

        pathfinderAppState = new PathfinderAppState();
        stateManager.attach(pathfinderAppState);

        AIAppState = new AIAppState();
        stateManager.attach(AIAppState);

        partAppState = new ParticleAppState();
        stateManager.attach(partAppState);

        startScreen = new InterfaceController();
        stateManager.attach(startScreen);

        levelManager = new LevelManager();
        stateManager.attach(levelManager);


        //text.initCrossHairs(guiNode, guiFont, assetManager, settings);

        //bulletAppState.getPhysicsSpace().enableDebug(assetManager);
       
        setUpLight();
        menu.createMenu(this);


    }

    private void setUpLight() {
        AmbientLight al = new AmbientLight();
        al.setColor(ColorRGBA.White);
        rootNode.addLight(al);

        DirectionalLight sun = new DirectionalLight();
        lightDir = new Vector3f(-0.37352666f, -0.50444174f, -0.7784704f);
        sun.setDirection(lightDir);
        sun.setColor(ColorRGBA.White);
        rootNode.addLight(sun);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setTexture("ColorMap", assetManager.loadTexture("Interface/Logo/Monkey.jpg"));
        Sphere lite = new Sphere(8, 8, 3.0f);
        Geometry lightSphere = new Geometry("lightsphere", lite);
        lightSphere.setMaterial(mat);
        Vector3f lightPos = lightDir.multLocal(-3000);
        lightSphere.setLocalTranslation(lightPos);
        rootNode.attachChild(lightSphere);

        pssmRenderer = new PssmShadowRenderer(assetManager, 1024, 3);
        pssmRenderer.setDirection(lightDir.mult(-1));
        pssmRenderer.setLambda(5f);
        pssmRenderer.setShadowIntensity(0.6f);
        pssmRenderer.setCompareMode(CompareMode.Hardware);
        pssmRenderer.setFilterMode(FilterMode.Dither);
        viewPort.addProcessor(pssmRenderer);

//        fpp=new FilterPostProcessor(assetManager);
//        bloom = new BloomFilter(BloomFilter.GlowMode.Objects);  
//        bloom.setExposurePower(20f);
//        fpp.addFilter(bloom);
//        
//        viewPort.addProcessor(fpp);



//        pssmFilter = new PssmShadowFilter(assetManager, 1024, 3);
//        pssmFilter.setDirection(new Vector3f(-1, -1, -1).normalizeLocal());
//        //pssmRenderer.setDirection(new Vector3f(0.5973172f, -0.16583486f, 0.7846725f).normalizeLocal());
//        pssmFilter.setLambda(0.55f);
//        pssmFilter.setShadowIntensity(0.6f);
//        pssmFilter.setCompareMode(CompareMode.Hardware);
//        pssmFilter.setFilterMode(FilterMode.Dither);
//        pssmFilter.setEnabled(false);
//        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
//        //  fpp.setNumSamples(4);
//        fpp.addFilter(pssmFilter);
//       
//        viewPort.addProcessor(fpp);

//        fpp = new FilterPostProcessor(assetManager);
//        //fpp.setNumSamples(4);
//        fog = new FogFilter();
//        fog.setFogColor(new ColorRGBA(0.8f, 0.8f, 1.0f, 1.0f));
//        fog.setFogDistance(400);
//        fog.setFogDensity(1.4f);
//        fpp.addFilter(fog);
//        
//        viewPort.addProcessor(fpp);

//       PssmShadowRenderer pssmRenderer = new PssmShadowRenderer(assetManager,1024,4);
//        pssmRenderer.setDirection(lightDir);
//        //pssmRenderer.setShadowIntensity(.1f);
//       // pssmRenderer.setDirection(lightDir);
//        pssmRenderer.setLambda(.55f);
//        pssmRenderer.setShadowIntensity(0.6f);
//        pssmRenderer.setCompareMode(CompareMode.Hardware);
//        pssmRenderer.setFilterMode(FilterMode.Bilinear);
//        viewPort.addProcessor(pssmRenderer);

        //fpp = new FilterPostProcessor(assetManager);
//        SSAOFilter ssaoFilter= new SSAOFilter(12.940201f, 43.928635f, 0.32999992f, 0.6059958f);
//        fpp.addFilter(ssaoFilter);

        //LightScatteringFilter filter = new LightScatteringFilter(lightPos);
        //filter.setLightDensity(.8f);
        //filter.setNbSamples(15);
        //LightScatteringUI Lui = new LightScatteringUI(inputManager, filter);
        //fpp.addFilter(filter);
        //SSAOUI Sui = new SSAOUI(inputManager, ssaoFilter);
        //viewPort.addProcessor(fpp);
    }

    private void setUpWater() {
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        final WaterFilter water = new WaterFilter(rootNode, lightDir);
        water.setWaterHeight(-50);
        water.setUseFoam(true);
        water.setUseRipples(true);
        water.setDeepWaterColor(ColorRGBA.Brown);
        water.setWaterColor(ColorRGBA.Brown.mult(2.0f));
        water.setWaterTransparency(0.2f);
        water.setMaxAmplitude(0.3f);
        water.setWaveScale(0.008f);
        water.setSpeed(0.7f);
        water.setShoreHardness(1.0f);
        water.setRefractionConstant(0.2f);
        water.setShininess(0.3f);
        water.setSunScale(1.0f);
        water.setColorExtinction(new Vector3f(10.0f, 20.0f, 30.0f));
        fpp.addFilter(water);
        viewPort.addProcessor(fpp);
    }

}
