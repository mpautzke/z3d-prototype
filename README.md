# README #

### What is this repository for? ###

This is the prototype (single player) version of Zombie3D

### How do I get set up? ###

- Pull repo down to your development folder
- Download and install [JME IDE](http://jmonkeyengine.org/downloads/) (Netbeans wrapper)
- Open the project in the JME IDE
- A popup will display asking you to resolve dependencies for Junit.  Select resolve dependencies.  Then select resolve and follow instructions
- At this point you should be able to build and run Zombie3D

### Other Versions ###

There is a multi-player version repo located here: [Prototype Zombie3D](https://bitbucket.org/mpautzke/z3d2)