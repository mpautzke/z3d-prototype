/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.system.AppSettings;
import com.jme3.font.BitmapText;
import com.jme3.scene.Node;
import com.jme3.font.BitmapFont;
import com.jme3.asset.AssetManager;
/**
 *
 * @author Matt
 */
public class Text {

    protected void initCrossHairs(Node guiNode,BitmapFont guiFont,AssetManager assetManager,AppSettings settings) {
    guiNode.detachAllChildren();
    guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
    BitmapText ch = new BitmapText(guiFont, false);
    ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
    ch.setText("+");        // fake crosshairs :)
    ch.setLocalTranslation( // center
      settings.getWidth() / 2 - guiFont.getCharSet().getRenderedSize() / 3 * 2,
      settings.getHeight() / 2 + ch.getLineHeight() / 2, 0);
    guiNode.attachChild(ch);
  }
}
