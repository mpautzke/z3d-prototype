/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Cylinder;

/**
 *
 * @author Matt
 */
public class BlltComponent implements PhysicsCollisionListener {

    private SimpleApplication app;
    private BulletAppState bullet;
    private RigidBodyControl blt;
    private CollisionShape bulletCollisionShape;
    private Cylinder cylinder;
    private Materials mat;
    private float Duration = 0;
    private Spatial bllt;
    private Spatial missile;
    private Quaternion view = new Quaternion();

    public BlltComponent(Application app, BulletAppState bullet) {
        
        this.app = (SimpleApplication) app;
        this.bullet = bullet;
        mat = new Materials(app.getAssetManager());
    }

    public void Bullet(Vector3f Location, Vector3f Direction) {
        /** Create a cannon ball geometry and attach to scene graph. */
        cylinder = new Cylinder(10, 10, .01f, 2f,true, false);
        bllt = new Geometry("bullet", cylinder);
        bllt.setMaterial(mat.getTracer().clone());
        view.lookAt(Direction, Vector3f.UNIT_Y);
        bllt.setLocalRotation(view);
        bllt.setShadowMode(ShadowMode.Off);
        bllt.setLocalTranslation(Location.add(Direction.mult(2)));
        /** Make the ball physcial with a mass > 0.0f */
        bulletCollisionShape = CollisionShapeFactory.createDynamicMeshShape(bllt);
        //bulletCollisionShape = new SphereCollisionShape(bulletSize);
        blt = new RigidBodyControl(bulletCollisionShape, 1);
        blt.setCcdMotionThreshold(.01f);
        blt.setLinearVelocity(Direction.mult(500));
        bllt.addControl(blt);
        blt.setCollisionGroup(9);
        blt.removeCollideWithGroup(9);
        app.getRootNode().attachChild(bllt);
        bullet.getPhysicsSpace().add(bllt);
        bullet.getPhysicsSpace().addCollisionListener(this);

    }
    
    public void bomb(Vector3f Location, Vector3f Direction){
        view.lookAt(Direction, Vector3f.UNIT_Y);
        Vector3f dir = view.getRotationColumn(2);
        
        missile = app.getAssetManager().loadModel("Models/SpaceCraft/Rocket.mesh.j3o");
        missile.scale(0.5f);
        missile.rotate(0, FastMath.PI, 0);
        missile.updateGeometricState();

        BoundingBox box = (BoundingBox) missile.getWorldBound();
        final Vector3f extent = box.getExtent(null);

        BoxCollisionShape boxShape = new BoxCollisionShape(extent);

        missile.setName("Missile");
        missile.rotate(view);
        missile.setLocalTranslation(Location.add(Direction.mult(2)));
        missile.setLocalRotation(view);
        missile.setShadowMode(ShadowMode.Cast);
        RigidBodyControl control = new BombControl(app.getAssetManager(), boxShape, 20);
        control.setLinearVelocity(dir.mult(100));
        control.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_03);
        missile.addControl(control);


        app.getRootNode().attachChild(missile);
        bullet.getPhysicsSpace().add(missile);
    }

    public void setDuration(float tpf) {
        Duration += tpf;
        if (Duration > 2) {
            if (bllt != null){
            app.getRootNode().detachChild(bllt);
            bullet.getPhysicsSpace().remove(bllt);
            }
            if (missile != null){
            app.getRootNode().detachChild(missile);
            bullet.getPhysicsSpace().remove(missile);
            }
        }
    }



    public void collision(PhysicsCollisionEvent event) {

        switch (event.getType()) {

            case PhysicsCollisionEvent.TYPE_PROCESSED:
                final Spatial nodeA = event.getNodeA();
                final Spatial nodeB = event.getNodeB();

                if (nodeA == null || nodeB == null) {
                    return;
                }

                if (bllt.equals(event.getNodeA()) || bllt.equals(event.getNodeB())) {
                    
                    Duration = 3;

                }


                break;
        }
    }
        public float getDuration() {
        return Duration;
    }
}
