package mygame;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.ChaseCamera;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 *
 */
public class InterfaceController extends AbstractAppState implements ScreenController {

    private Nifty nifty;
    private SimpleApplication app;
    private Screen screen;
    private Screen prev;
    public ChaseCamera chaseCam;
    private Element health;
    private Element kills;
    private Element level;

    /**
     * custom methods
     */
    
    public InterfaceController() {
        /**
         * You custom constructor, can accept arguments
         */
    }
    

    public void startGame(String nextScreen) {
        if (!screen.getScreenId().equals(nextScreen)) {
            nifty.gotoScreen(nextScreen);  // switch to another screen
            screen = nifty.getScreen(nextScreen);
        }
    }

    public void ButtonSound() {
        Main.audio.getButton().play();
    }

    public void quitGame() {
        app.stop();
    }

    public String getPlayerName() {
        return System.getProperty("user.name");
    }

    /**
     * Nifty GUI ScreenControl methods
     */
    public void bind(Nifty nifty, Screen screen) {
        this.nifty = nifty;
        this.screen = screen;
        if (screen.getScreenId().equals("hud")) {
            Main.audio.getMenu().stop();
            Main.audio.getGame().play();
        }
        if (screen.getScreenId().equals("menu")) {
            Main.audio.getGame().stop();
            Main.audio.getMenu().play();
        }
    }

    public void onStartScreen() {
    }

    public void onEndScreen() {
    }

    public String prevScreen() {
        return prev.toString();
    }

    public String Screen() {
        return screen.toString();
    }

    /**
     * jME3 AppState methods
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        this.app = (SimpleApplication) app;
        health = nifty.getScreen("hud").findElementByName("health");
        kills = nifty.getScreen("hud").findElementByName("kills");
        level = nifty.getScreen("hud").findElementByName("level");
    }

    @Override
    public void update(float tpf) {
        if (screen.getScreenId().equals("hud")) {
            if (app.getFlyByCamera().isDragToRotate()) {
                app.getFlyByCamera().setDragToRotate(false);
            }
            if (!app.getStateManager().getState(BlltAppState.class).isEnabled()) {
                app.getStateManager().getState(BlltAppState.class).setEnabled(true);
            }
            if (!app.getStateManager().getState(UserAppState.class).isEnabled()) {
                app.getStateManager().getState(UserAppState.class).setEnabled(true);
            }
            if (!app.getStateManager().getState(AIAppState.class).isEnabled()) {
                app.getStateManager().getState(AIAppState.class).setEnabled(true);
            }
            if (!app.getStateManager().getState(ParticleAppState.class).isEnabled()) {
                app.getStateManager().getState(ParticleAppState.class).setEnabled(true);
            }
            if (!app.getStateManager().getState(BulletAppState.class).isEnabled()) {
                app.getStateManager().getState(BulletAppState.class).setEnabled(true);
            }
        } else if (screen.getScreenId().equals("menu")) {
            if (!app.getFlyByCamera().isDragToRotate()) {
                app.getFlyByCamera().setDragToRotate(true);
            }
            if (app.getStateManager().getState(BlltAppState.class).isEnabled()) {
                app.getStateManager().getState(BlltAppState.class).setEnabled(false);
            }
            if (app.getStateManager().getState(UserAppState.class).isEnabled()) {
                app.getStateManager().getState(UserAppState.class).setEnabled(false);
            }
            if (app.getStateManager().getState(AIAppState.class).isEnabled()) {
                app.getStateManager().getState(AIAppState.class).setEnabled(false);
            }
            if (app.getStateManager().getState(ParticleAppState.class).isEnabled()) {
                app.getStateManager().getState(ParticleAppState.class).setEnabled(false);
            }
            if (app.getStateManager().getState(BulletAppState.class).isEnabled()) {
                app.getStateManager().getState(BulletAppState.class).setEnabled(false);
            }

        } else if (screen.getScreenId().equals("pause")) {
            if (!app.getFlyByCamera().isDragToRotate()) {
                app.getFlyByCamera().setDragToRotate(true);
            }
            if (app.getStateManager().getState(BlltAppState.class).isEnabled()) {
                app.getStateManager().getState(BlltAppState.class).setEnabled(false);
            }
            if (app.getStateManager().getState(UserAppState.class).isEnabled()) {
                app.getStateManager().getState(UserAppState.class).setEnabled(false);
            }
            if (app.getStateManager().getState(AIAppState.class).isEnabled()) {
                app.getStateManager().getState(AIAppState.class).setEnabled(false);
            }
            if (app.getStateManager().getState(ParticleAppState.class).isEnabled()) {
                app.getStateManager().getState(ParticleAppState.class).setEnabled(false);
            }
            if (app.getStateManager().getState(BulletAppState.class).isEnabled()) {
                app.getStateManager().getState(BulletAppState.class).setEnabled(false);
            }
        } else if (screen.getScreenId().equals("death")) {
            if (chaseCam == null) {
                chaseCam = new ChaseCamera(app.getCamera(), app.getStateManager().getState(UserAppState.class).getPlayerModel(), app.getInputManager());
            }

        }
        if (health != null){
        health.getRenderer(TextRenderer.class).setText(Integer.toString(app.getStateManager().getState(UserAppState.class).getHealth()));
        kills.getRenderer(TextRenderer.class).setText(Integer.toString(app.getStateManager().getState(AIAppState.class).getKills()));
        level.getRenderer(TextRenderer.class).setText(Integer.toString(app.getStateManager().getState(AIAppState.class).getLevel()));
        }
        
    }
}
