/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.scene.Spatial;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.*;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import jme3tools.navmesh.NavMesh;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.util.SkyFactory;
import forester.Forester;
import java.util.LinkedList;
import java.util.List;

import java.util.ListIterator;

/**
 *
 * @author Matt
 */
public class Scene extends AbstractAppState{
    private SimpleApplication app;
    private AppStateManager stateManager;
    private Node mainScene = new Node("MainScene");
    private Node sceneModel;
    private RigidBodyControl landscape;
    private NavMesh navMesh;
    private Vector3f lightDir;
    private Forester forester;
    private LinkedList<Vector3f> spawnList = new LinkedList();
    private LinkedList<Vector3f> pSpawnList = new LinkedList();

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication) app;
        this.stateManager = stateManager;
        setUpArena();
        
    }

    public void setUpArena() {
        sceneModel = (Node) app.getAssetManager().loadModel("Scenes/Arena/Arena.j3o");
        sceneModel.setName("Scene");
        Geometry navMeshGeom = (Geometry) sceneModel.getChild("NavMesh");
        navMesh = null;
        if (navMeshGeom != null) {
            navMesh = new NavMesh(navMeshGeom.getMesh());
        }
        sceneModel.detachChild(navMeshGeom);
        //Add zombie spawns to list
        List<Spatial> children = sceneModel.getChildren();
        for (ListIterator<Spatial> child = children.listIterator(); child.hasNext();) {
            Spatial spawn = child.next();
            if (spawn.getName().matches(".*Spawn.*")){
                spawnList.add(spawn.getWorldTranslation());
            }
            if (spawn.getName().matches(".*Player.*")){
                pSpawnList.add(spawn.getWorldTranslation());
            }
 
            }

        CollisionShape sceneShape = CollisionShapeFactory.createMeshShape((Node) sceneModel);
        landscape = new RigidBodyControl(sceneShape, 0);
        sceneModel.addControl(landscape);
        sceneModel.setShadowMode(ShadowMode.CastAndReceive);
        Spatial sky = SkyFactory.createSky(app.getAssetManager(), "Textures/Sky/Bright/BrightSky.dds", false);
        app.getRootNode().attachChild(sceneModel);
        app.getRootNode().attachChild(sky);
        
        stateManager.getState(BulletAppState.class).getPhysicsSpace().add(landscape);
       
    }

    
//        public void setupForester(){
//        Node ter = (Node) scene.getSceneModel();
//        terrain = (TerrainQuad) ter.getChild("terrain");
//        TerrainLodControl control = new TerrainLodControl(terrain, getCamera());
//        control.setLodCalculator(new DistanceLodCalculator(65, 2.7f)); // patch size, and a multiplier
//        terrain.addControl(control);
//        ter.setShadowMode(ShadowMode.Receive);
//
//        rootNode.attachChild(ter);
//        
//        CollisionShape terrainShape =
//        CollisionShapeFactory.createMeshShape((Node) ter);
//        RigidBodyControl landscape = new RigidBodyControl(terrainShape, 0);
//        terrain.addControl(landscape);
//        bulletAppState.getPhysicsSpace().add(landscape);
//        
//       
//        // Step 1 - set up the forester. The forester is a singleton class that
//        // can be accessed statically from anywhere, but we use a reference
//        // variable here.
//        forester = Forester.getInstance();
//        forester.initialize(rootNode, cam, terrain, null, this);
//        forester.getForesterNode().setLocalTranslation(terrain.getLocalTranslation());
//        //forester.getForesterNode().setLocalScale(terrain.getLocalScale());
//
//        // Step 2 - set up the treeloader. Page size is the same size as the
//        // scaled terrain. Resolution is 4, meaning there are 4x4 = 16 blocks
//        // of tree geometry per page (16 batches).
//        // 
//        // Far viewing range is set to 800, so that you can see the tree-blocks
//        // being added and removed. Increase it by 100 or so and the trees will 
//        // be added/removed seamlessly (no popping).
//        
//        TreeLoader treeLoader = forester.createTreeLoader(1026, 10, 900f);
//        
//
//        Texture density = terrain.getMaterial().getTextureParam("AlphaMap").getTextureValue();
//        
//        // Step 3 - set up the datagrid.
//        forester.trees.datagrids.MapGrid mapGrid = treeLoader.createMapGrid();
//        mapGrid.addDensityMap(density, 0, 0, 0);
//        // Set the densitymap value threshold. Any density values lower then
//        // the set value will be discarded.
//        mapGrid.setThreshold(.5f);
//
//        // Step 4 - set up a tree layer
//        Spatial model = assetManager.loadModel("Models/Spruce/SpruceMediumPoly.j3o");
////        List<Spatial> modelChildren = ((Node)model).getChildren();
////        ((Geometry)modelChildren.get(0)).setMaterial(assetManager.loadMaterial("Models/Spruce/Trunk.j3m"));
////        ((Geometry)modelChildren.get(1)).setMaterial(assetManager.loadMaterial("Models/Spruce/Branches.j3m"));
//
//        // Create a tree-layer and configure it. The density texture data and
//        // density multiplier works as described in SimpleGrassTest.
//        TreeLayer treeLayer = treeLoader.addTreeLayer(model, false);
//        treeLayer.setDensityTextureData(0, Channel.Red);
//        treeLayer.setDensityMultiplier(1f);
//
//        treeLayer.setMaximumScale(7f);
//        treeLayer.setMinimumScale(1.3f);
//
//        //Adding some grass as well.
//        GrassLoader grassLoader = forester.createGrassLoader(1026, 10, 300f, 20f);
//
//        MapGrid grid = grassLoader.createMapGrid();
//
//        grid.addDensityMap(density, 0, 0, 0);
//
//        Material grassMat = assetManager.loadMaterial("Materials/Grass/Grass.j3m");
//
//        GrassLayer layer = grassLoader.addLayer(grassMat, MeshType.CROSSQUADS);
//
//        layer.setDensityTextureData(0, Channel.Red);
//
//        layer.setDensityMultiplier(.3f);
//
//        layer.setMaxHeight(2.4f);
//        layer.setMinHeight(2.f);
//
//        layer.setMaxWidth(2.4f);
//        layer.setMinWidth(2.f);
//
//        layer.setMaxTerrainSlope(30);
//
//        ((GPAUniform) layer.getPlantingAlgorithm()).setThreshold(0.5f);
//
//        grassLoader.setWind(new Vector2f(1, 0));
//        
//    }
    


    public Spatial getSceneModel() {
        return sceneModel;
    }

    public NavMesh getNavMesh() {
        return navMesh;
    }
    
    public LinkedList<Vector3f> getSpawnList(){
        return spawnList;
    }
    
    public LinkedList<Vector3f> getPlayerSpawn(){
        return pSpawnList;
    }
    
}
