/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;
import com.jme3.material.Material;
import com.jme3.asset.AssetManager;
import com.jme3.asset.TextureKey;
import com.jme3.math.ColorRGBA;
import com.jme3.texture.Texture;
/**
 *
 * @author Matt
 */
public class Materials {
    private Material stone_mat;
    private Material tracer;
      
    public Materials(AssetManager assetManager) {
    stone_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    TextureKey key2 = new TextureKey("Textures/Terrain/Rock/Rock.PNG");
    key2.setGenerateMips(true);
    Texture tex2 = assetManager.loadTexture(key2);
    stone_mat.setTexture("ColorMap", tex2);
    
    
    tracer = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    //tracer.setTexture("ColorMap", assetManager.loadTexture("Textures/ColoredTex/Monkey.png"));
    //tracer.setColor("Specular",ColorRGBA.Yellow);
    tracer.setColor("Color",ColorRGBA.Yellow);
    tracer.setColor("GlowColor", ColorRGBA.Yellow);
   }
    
    public Material getStone(){
        return stone_mat;
    }
    
    public Material getTracer(){
        return tracer;
    }
}
