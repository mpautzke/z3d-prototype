/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

//import com.jme3.bullet.BulletAppState;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.*;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.MouseInput;
import com.jme3.input.InputManager;
import com.jme3.input.controls.MouseButtonTrigger;

/**
 *
 * @author Matt
 */
public class Keyboard implements ActionListener {

    private boolean left = false, right = false, up = false, down = false;
    private boolean jump = false, shoot = false, aim = false, pause = false;
    private String gun;

    public void setUpKeys(InputManager inputManager) {
        inputManager.addMapping("CharLeft", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("CharRight", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("CharUp", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("CharDown", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("CharSpace", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("CharShoot", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addMapping("CharAim", new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        inputManager.addMapping("GamePause", new KeyTrigger(KeyInput.KEY_P));
        inputManager.addMapping("1", new KeyTrigger(KeyInput.KEY_1));
        inputManager.addMapping("2", new KeyTrigger(KeyInput.KEY_2));
        inputManager.addListener(this, "CharLeft");
        inputManager.addListener(this, "CharRight");
        inputManager.addListener(this, "CharUp");
        inputManager.addListener(this, "CharDown");
        inputManager.addListener(this, "CharSpace");
        inputManager.addListener(this, "CharShoot");
        inputManager.addListener(this, "CharAim");
        inputManager.addListener(this, "GamePause");
        inputManager.addListener(this, "1");
        inputManager.addListener(this, "2");


    }

    public void onAction(String binding, boolean value, float tpf) {
        if (binding.equals("CharLeft")) {
            if (value) {
                left = true;
            } else {
                left = false;
            }
        } else if (binding.equals("CharRight")) {
            if (value) {
                right = true;
            } else {
                right = false;
            }
        } else if (binding.equals("CharUp")) {
            if (value) {
                up = true;
            } else {
                up = false;
            }
        } else if (binding.equals("CharDown")) {
            if (value) {
                down = true;
            } else {
                down = false;
            }
        } else if (binding.equals("CharSpace")) {
            if (value) {
                jump = true;
            } else {
                jump = false;
            }
        } else if (binding.equals("CharShoot") && value) {
            shoot = true;
        } else if (binding.equals("CharAim") && value) {
            if (value) {
                aim = !aim;
            }

        } else if (binding.equals("GamePause") && value) {
            if (value) {
                pause = !pause;
            }

        } else {

            if (binding.equals("1") && value) {
                gun = "pistol";
            } else if (binding.equals("2") && value) {
                gun = "shotgun";
            }

        }

    }

    public boolean isDown() {
        return down;
    }

    public void setDown(boolean down) {
        this.down = down;
    }

    public boolean isJump() {
        return jump;
    }

    public void setJump(boolean jump) {
        this.jump = jump;
    }

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public boolean isShoot() {
        return shoot;
    }

    public void setShoot(boolean shoot) {
        this.shoot = shoot;
    }

    public boolean isUp() {
        return up;
    }

    public void setUp(boolean up) {
        this.up = up;
    }

    public boolean isAim() {
        return aim;
    }

    public void setAim(boolean aim) {
        this.aim = aim;
    }

    public boolean isPause() {
        return pause;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }
    public String getGun(){
        return gun;
    }
    public void setGun(String gun){
        this.gun = gun;
    }
}
