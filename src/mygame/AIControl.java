/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.bounding.BoundingVolume;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.collision.CollisionResults;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import java.util.List;

/**
 *
 * @author Matt
 */
public class AIControl extends AbstractControl implements AnimEventListener {

    private SimpleApplication app;
    private AudioNode run;
    private RigidBodyControl physicsCharacter;
    private Vector3f Gravity = new Vector3f(0, -50f, 0);
    private float duration = 0;
    private List<Vector3f> path;
    private int index;
    private Vector3f Point1 = new Vector3f(0, 0, 0);
    private Vector3f Point2 = new Vector3f(0, 0, 0);
    private Vector3f direction = new Vector3f(0, 0, 0);
    private float distance = 0;
    private Quaternion view = new Quaternion();
    private Vector3f speed = new Vector3f(0, 0, 0);
    private Vector3f walkDirection = new Vector3f(0, 0, 0);
    private Vector3f viewDirection = new Vector3f(0, 0, 0);
    private Vector3f Position = new Vector3f(0, 0, 0);
    private Node Model;
    private AnimControl animControl;
    private AnimChannel animChannel;
    private AnimChannel attackChannel;
    private float attackDuration;
    Vector3f TDPos = new Vector3f(Vector3f.ZERO);

    public AIControl() {
    }
   
    public AIControl(Vector3f Position, SimpleApplication app, Node Model) {
        this.app = app;
        this.Model = Model;
        this.Position = Position;
        createCapsule();
        setupAnimationController();
        run = Main.audio.getRun().clone();
        Model.attachChild(run);
        
    }

    @Override
    protected void controlUpdate(float tpf) {
        duration += tpf;
        if (duration > 1f) {
            path = app.getStateManager().getState(PathfinderAppState.class).findPath(getPosition(), app.getStateManager().getState(UserAppState.class).getPosition());
            index = 0;
            duration = 0;
        }

        Move(tpf);
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        final AIControl control = new AIControl();
        spatial.addControl(physicsCharacter);
        /* Optional: use setters to copy userdata into the cloned control */
        // control.setIndex(i); // example
        control.setSpatial(spatial);
        return control;
    }

    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        if (spatial == null) {
            this.app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().remove(physicsCharacter);
            if (run.getStatus().equals(AudioNode.Status.Playing)){
                    run.stop();
            }
            return;
        }
        spatial.addControl(physicsCharacter);
        physicsCharacter.setPhysicsLocation(Position);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        
    }

    public void Move(float tpf) {
        attackDuration += tpf;
        if (path.size() > 0) {
            Point1.x = path.get(index).x;
            Point1.z = path.get(index).z;
            Point2.x = physicsCharacter.getPhysicsLocation().x;
            Point2.z = physicsCharacter.getPhysicsLocation().z;
            
            direction = Point1.subtract(Point2).normalize();
            distance = physicsCharacter.getPhysicsLocation().distance(path.get(index));
            if (distance > 10000){
                this.setEnabled(false);
            }
            viewDirection.addLocal(direction.subtract(viewDirection).mult(5f * tpf));

            
            view = physicsCharacter.getPhysicsRotation().clone();
            view.lookAt(viewDirection, Vector3f.UNIT_Y);
            speed.set(7 * FastMath.cos(viewDirection.x * viewDirection.z), 0,0);
            float tempdistance = physicsCharacter.getPhysicsLocation().distance(path.get(path.size()-1));
            if (tempdistance < 10){
                index = path.size()-1;
            }
            

            if (index + 1 == path.size() && distance < 7) {
                view.lookAt(app.getStateManager().getState(UserAppState.class).getPosition().subtract(physicsCharacter.getPhysicsLocation()), Vector3f.UNIT_Y);
                walkDirection.set(0, 0, 0);
                physicsCharacter.setAngularVelocity(walkDirection);
                physicsCharacter.setPhysicsRotation(view);

                if (!"Attack".equals(attackChannel.getAnimationName())) {
                    attackChannel.setAnim("Attack", .5f);
                }
                if (!"Attack".equals(animChannel.getAnimationName())) {
                    animChannel.setAnim("Attack", .5f);
                }
                if (run.getStatus().equals(AudioNode.Status.Playing)){
                    run.pause();
                }

            } else if ((index + 1 != path.size()) && distance < 7) {
                walkDirection.addLocal(viewDirection);
                physicsCharacter.setAngularVelocity(view.mult(speed));
                physicsCharacter.setPhysicsRotation(view);
                if (index + 1 != path.size()) {
                    index++;
                }
                
                if (!"Run".equals(animChannel.getAnimationName())) {
                    animChannel.setAnim("Run", 1f);
                }
                if (!"Run".equals(attackChannel.getAnimationName())) {
                    attackChannel.setAnim("Run", .5f);
                }
                if (!run.getStatus().equals(AudioNode.Status.Playing)){
                    run.play();
                }

            } else if (distance >= 7) {
                walkDirection.addLocal(viewDirection);
                physicsCharacter.setAngularVelocity(view.mult(speed));
                physicsCharacter.setPhysicsRotation(view);
                if (!"Run".equals(animChannel.getAnimationName())) {
                    animChannel.setAnim("Run", 1f);
                }
                if (!"Run".equals(attackChannel.getAnimationName())) {
                    attackChannel.setAnim("Run", .5f);
                }
                if (!run.getStatus().equals(AudioNode.Status.Playing)){
                    run.play();
                }


            }


        } else {
            walkDirection.set(0, 0, 0);
            physicsCharacter.setAngularVelocity(view.mult(walkDirection));
            physicsCharacter.setPhysicsRotation(view);
            if (!"Idle".equals(animChannel.getAnimationName())) {
                animChannel.setAnim("Idle", 1f);
            }
            if (!"Idle".equals(attackChannel.getAnimationName())) {
                attackChannel.setAnim("Idle", 1f);
            }
            if (run.getStatus().equals(AudioNode.Status.Playing)){
                    run.pause();
            }

        }

        if ("Attack".equals(attackChannel.getAnimationName())) {

            if (attackChannel.getTime() > .2) {
                CollisionResults results = new CollisionResults();
                BoundingVolume bv = Model.getWorldBound();
                app.getStateManager().getState(UserAppState.class).getPlayer().collideWith(bv, results);
                if (results.size() > 0 && attackDuration >= 3) {
                    app.getStateManager().getState(UserAppState.class).setHealth(10);
                    System.out.println(duration);
                    attackDuration = 0;

                }
            }
        }
    }

    private void createCapsule() {
        physicsCharacter = new RigidBodyControl(new CapsuleCollisionShape(1.5f, 6f, 1), 5f);
        physicsCharacter.setAngularFactor(0);
        physicsCharacter.setFriction(15);
        this.app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().add(physicsCharacter);
        physicsCharacter.setGravity(Gravity);
        path = this.app.getStateManager().getState(PathfinderAppState.class).findPath(Position, this.app.getStateManager().getState(UserAppState.class).getPosition());

    }

    private void setupAnimationController() {
        animControl = Model.getControl(AnimControl.class);
        animControl.addListener(this);
        animChannel = animControl.createChannel();
        attackChannel = animControl.createChannel();
        attackChannel.addBone(animControl.getSkeleton().getBone("Head"));
        attackChannel.addBone(animControl.getSkeleton().getBone("Humerus.L"));
        attackChannel.addBone(animControl.getSkeleton().getBone("Humerus.R"));
        attackChannel.addBone(animControl.getSkeleton().getBone("Ulna.L"));
        attackChannel.addBone(animControl.getSkeleton().getBone("Ulna.R"));

        animChannel.addBone(animControl.getSkeleton().getBone("Thigh.L"));
        animChannel.addBone(animControl.getSkeleton().getBone("Thigh.R"));
        animChannel.addBone(animControl.getSkeleton().getBone("Calf.L"));
        animChannel.addBone(animControl.getSkeleton().getBone("Calf.R"));
        animChannel.addBone(animControl.getSkeleton().getBone("Chest"));

    }

    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
    }

    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
    }

    public Vector3f getTDPosition() {
        TDPos = new Vector3f(Vector3f.ZERO);
        TDPos.x = physicsCharacter.getPhysicsLocation().x;
        TDPos.z = physicsCharacter.getPhysicsLocation().z;
        return TDPos;
    }

    public Vector3f getPosition() {
        return physicsCharacter.getPhysicsLocation();
    }
}
