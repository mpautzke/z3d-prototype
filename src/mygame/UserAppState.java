/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.SkeletonControl;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioSource;
import com.jme3.bullet.*;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.KinematicRagdollControl;
import com.jme3.math.*;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.Random;

/**
 *
 * @author Matt
 */
public class UserAppState extends AbstractAppState implements PhysicsCollisionListener, AnimEventListener {

    private GunManager gun = new GunManager();
    private SimpleApplication app;
    private BulletAppState bullet;
    private CharacterControl playerControl;
    private KinematicRagdollControl ragdoll;
    private Node playerModel;
    private Node player;
    private AnimControl animControl;
    private AnimChannel animChannel;
    private AnimChannel aimChannel;
    private Vector3f walkDirection = new Vector3f();
    private float airTime = 0;
    private int Health = 100;
    private boolean death = false;
    private SkeletonControl skelControl;
    private Node Head;
    private AudioNode run;
    private Vector3f Spawn = new Vector3f(0,0,0);
    private int mod;
    private Random rand = new Random();

    @Override
    public void initialize(AppStateManager manager, Application app) {

        super.initialize(manager, app);
        this.app = (SimpleApplication) app;
        
        mod = this.app.getStateManager().getState(Scene.class).getPlayerSpawn().size();
        if (mod > 0){
        int index = (Math.abs(rand.nextInt() % mod));
        Spawn = this.app.getStateManager().getState(Scene.class).getPlayerSpawn().get(index).clone();
        } else{
            Spawn.set(0,50,0);
        }
        
        this.bullet = manager.getState(BulletAppState.class);
        run = Main.audio.getRun().clone();
        setUpCharacter();
        gun.GunManager(app, manager);
        gun.setGun("pistol");
        Main.keyboard.setGun("pistol");
        
        

    }

    @Override
    public void update(float tpf) {
        if (playerControl.isEnabled()) {
            Vector3f camDir = app.getCamera().getDirection().clone().multLocal(.5f);
            Vector3f camLeft = app.getCamera().getLeft().clone().multLocal(.5f);
            camDir.y = 0;
            camLeft.y = 0;
            walkDirection.set(0, 0, 0);
            
            if (Main.keyboard.isLeft()) {
                walkDirection.addLocal(camLeft);
            }
            if (Main.keyboard.isRight()) {
                walkDirection.addLocal(camLeft.negate());
            }
            if (Main.keyboard.isUp()) {
                walkDirection.addLocal(camDir);
            }
            if (Main.keyboard.isDown()) {
                walkDirection.addLocal(camDir.negate());
            }
            if (Main.keyboard.isJump()) {
                playerControl.jump();
            }
            if (Main.keyboard.isShoot() && Main.keyboard.isAim()) {
                gun.shoot();
                Main.keyboard.setShoot(false);


            } else {
                Main.keyboard.setShoot(false);
            }

            if (!playerControl.onGround()) {
                //System.out.println("not on ground");
                airTime = airTime + tpf;
                if (run.getStatus().equals(AudioSource.Status.Playing)) {
                    run.pause();
                }
            } else {
                airTime = 0;
            }

            if (walkDirection.length() == 0) {
                if (!"Idle".equals(animChannel.getAnimationName())) {
                    animChannel.setAnim("Idle", 1f);
                }
                if (run.getStatus().equals(AudioSource.Status.Playing)) {
                    run.pause();
                }

            } else {

                if (airTime > .3f) {
                    if (!"Run".equals(animChannel.getAnimationName())) {
                        animChannel.setAnim("Run");
                    }
                } else {
                    if (!"Run".equals(animChannel.getAnimationName())) {
                        animChannel.setAnim("Run", 0.7f);
                    }
                    if (playerControl.onGround() && !run.getStatus().equals(AudioSource.Status.Playing)) {
                        run.play();
                    }
                }
            }
            if (Main.keyboard.isAim()) {
                if (!"Aim".equals(aimChannel.getAnimationName())) {
                    aimChannel.setAnim("Aim", .5f);
                }
                aimChannel.setTime(app.getCamera().getDirection().y * .45f + .45f);
                walkDirection.multLocal(.5f);

            } else {
                if (walkDirection.length() == 0) {
                    if (!"Idle".equals(aimChannel.getAnimationName())) {
                        aimChannel.setAnim("Idle", 1f);
                    }
                    if (run.getStatus().equals(AudioSource.Status.Playing)) {
                        run.pause();
                    }
                } else {

                    if (!"Run".equals(aimChannel.getAnimationName())) {
                        aimChannel.setAnim("Run", 0.7f);
                    }
                    if (playerControl.onGround() && !run.getStatus().equals(AudioSource.Status.Playing)) {
                        run.play();
                    }

                }

            }
            if (!gun.getGun().equals(Main.keyboard.getGun())) {
                gun.setGun(Main.keyboard.getGun());
            }

            playerControl.setWalkDirection(walkDirection);
            playerControl.setViewDirection(camDir);
            playerModel.updateModelBound();
            app.getCamera().setLocation(playerControl.getPhysicsLocation().add(0, 2.7f, 0).add(camDir.mult(1f)));
            app.getListener().setLocation(playerControl.getPhysicsLocation());
            
        }

        if (Health <= 0) {
            if (playerControl.isEnabled() == true) {
                Main.audio.getScream().setLocalTranslation(playerControl.getPhysicsLocation().clone());
                app.getRootNode().attachChild(Main.audio.getScream());
                Main.audio.getScream().play();
                
                playerControl.setEnabled(false);
                ragdoll.setEnabled(true);
                ragdoll.setRagdollMode();
                death = true;
                app.getFlyByCamera().setEnabled(false);
                app.getStateManager().getState(InterfaceController.class).startGame("death");

            }

        }


    }

    public void setUpCharacter() {
        player = new Node("player");
        player.setUserData("Health", Health);

        playerModel = (Node) app.getAssetManager().loadModel("Models/ZombieA/Zombie.mesh.j3o");
        playerModel.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        playerModel.center();
        player.attachChild(playerModel);
        playerModel.setLocalTranslation(0f, -2f, 0f);

        ragdoll = new KinematicRagdollControl(.5f);
        playerModel.addControl(ragdoll);
        setupPlayer(ragdoll);
        bullet.getPhysicsSpace().add(ragdoll);
        ragdoll.setEnabled(false);

        playerControl = new CharacterControl(new CapsuleCollisionShape(1.5f, 6f, 1), 1f);
        player.addControl(playerControl);
        playerControl.setJumpSpeed(30f);
        playerControl.setFallSpeed(50f);
        playerControl.setGravity(40f);
        playerControl.setPhysicsLocation(Spawn);
        bullet.getPhysicsSpace().add(player);
        playerControl.setEnabled(true);
        app.getRootNode().attachChild(player);
        bullet.getPhysicsSpace().addCollisionListener(this);
        setupAnimationController();
        
        playerModel.attachChild(run);
        

    }

    private void setupPlayer(KinematicRagdollControl ragdoll) {

        ragdoll.addBoneName("Root");
        ragdoll.addBoneName("Chest");
        ragdoll.addBoneName("Head");
        ragdoll.addBoneName("Humerus.L");
        ragdoll.addBoneName("Humerus.R");
        ragdoll.addBoneName("Thigh.L");
        ragdoll.addBoneName("Thigh.R");
        ragdoll.addBoneName("Ulna.L");
        ragdoll.addBoneName("Ulna.R");
        ragdoll.addBoneName("Calf.L");
        ragdoll.addBoneName("Calf.R");


    }

    public Node getPlayer() {
        return player;
    }

    public Spatial getPlayerModel() {
        return playerModel;
    }

    public CharacterControl getPlayerControl() {
        return playerControl;
    }

    public void collision(PhysicsCollisionEvent event) {

        switch (event.getType()) {

            case PhysicsCollisionEvent.TYPE_PROCESSED:

//                    final Spatial nodeA = event.getNodeA();
//                    final Spatial nodeB = event.getNodeB();
//
//                    if (nodeA == null || nodeB == null) {
//                        return;
//                    }


        }
    }

    private void setupAnimationController() {
        animControl = playerModel.getControl(AnimControl.class);
        animControl.addListener(this);
        animChannel = animControl.createChannel();
        aimChannel = animControl.createChannel();
        aimChannel.addBone(animControl.getSkeleton().getBone("Head"));
        aimChannel.addBone(animControl.getSkeleton().getBone("Humerus.L"));
        aimChannel.addBone(animControl.getSkeleton().getBone("Humerus.R"));
        aimChannel.addBone(animControl.getSkeleton().getBone("Ulna.L"));
        aimChannel.addBone(animControl.getSkeleton().getBone("Ulna.R"));

        animChannel.addBone(animControl.getSkeleton().getBone("Thigh.L"));
        animChannel.addBone(animControl.getSkeleton().getBone("Thigh.R"));
        animChannel.addBone(animControl.getSkeleton().getBone("Calf.L"));
        animChannel.addBone(animControl.getSkeleton().getBone("Calf.R"));
        animChannel.addBone(animControl.getSkeleton().getBone("Chest"));



    }

    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
    }

    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
    }

    public Vector3f getPosition() {
        return playerControl.getPhysicsLocation().clone();
    }

    public int getHealth() {
        return Health;
    }

    public boolean getDeath() {
        return death;
    }

    public void setHealth(int damage) {
        Health -= damage;
    }
}
